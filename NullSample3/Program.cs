﻿using System;

namespace NullSample3
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            BaseProduct product = new Product();
            product = product.GetProductByProductCode("P0001");


            BaseProduct product1 = new Product();
            product1 = product1.GetProductByProductCode("");
        }
    }

    public abstract class BaseProduct
    {
        protected string ProductCode = string.Empty;

        public BaseProduct(string pCode)
        {
            ProductCode = pCode;
        }

        public BaseProduct GetProductByProductCode(string productCode)
        {
            if (IsProductAvailable(productCode))

                return new Product(productCode);

            return new NullProduct(string.Empty);
        }

        public abstract bool IsProductAvailable(string pCode);
    }

    class NullProduct : BaseProduct
    {
        public NullProduct(string pCode) : base(pCode) { }

        public override bool IsProductAvailable(string pCode)
        {
            return false;
        }
    }

    class Product : BaseProduct
    {
        public Product(string pCode = "") : base(pCode) { }

        public override bool IsProductAvailable(string pCode)
        {
            if (string.IsNullOrEmpty(pCode))
                return false;
            return true;
        }
    }
}