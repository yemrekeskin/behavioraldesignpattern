﻿using System;

namespace StrategySample4
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    public interface ITransferOperation
    {
        bool DoAccounting();

        void Transfer();
    }

    public class ForeignPayment
        : ITransferOperation
    {
        public bool DoAccounting()
        {
            throw new NotImplementedException();
        }

        public void Transfer()
        {
            throw new NotImplementedException();
        }
    }

    public class DomesticPayment
        : ITransferOperation
    {
        public bool DoAccounting()
        {
            throw new NotImplementedException();
        }

        public void Transfer()
        {
            throw new NotImplementedException();
        }
    }

    public class SepaPayment
        : ITransferOperation
    {
        public bool DoAccounting()
        {
            throw new NotImplementedException();
        }

        public void Transfer()
        {
            throw new NotImplementedException();
        }
    }

    public class Payment
    {
        private readonly ITransferOperation _transfer;

        public Payment(ITransferOperation transfer)
        {
            this._transfer = transfer;
        }

        public bool DoAccounting()
        {
            this._transfer.DoAccounting();
            return default(bool);
        }

        public void Transfer()
        {
            this._transfer.Transfer();
        }
    }
}