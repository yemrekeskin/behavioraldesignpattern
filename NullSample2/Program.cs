﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NullSample2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    public class StudentRepository
    {
        private static IList<AbstractStudent> studentRep = GetStudentsFromDataSource();
        private static IList<AbstractStudent> GetStudentsFromDataSource()
        {
            //gets the students from the datasource.
            return new List<AbstractStudent>();
        }

        public static AbstractStudent GetStudentByFirstName(string firstName)
        {
            return studentRep.Where(item => item.FirstName == firstName).ElementAtOrDefault(0).GetNull();
        }
    }

    public static class Extensions
    {
        public static AbstractStudent GetNull(this AbstractStudent student)
        {
            return student == null ? AbstractStudent.Null : student;
        }
    }

    public abstract class AbstractStudent
    {
        public abstract string FirstName { get; set; }
        public abstract string LastName { get; set; }

        public abstract string FullName();

        public static readonly NullStudent Null = NullStudentInst;

        private static NullStudent NullStudentInst
        {
            get
            {
                return new NullStudent();
            }
        }

        public class NullStudent
            : AbstractStudent
        {
            public override string FirstName { get; set; }
            public override string LastName { get; set; }

            public override string FullName()
            {
                return string.Empty;
            }
        }
    }
}