﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterpreterSample3
{
    // TerminalExpression
    class DeveloperExpression
        : RoleExpression
    {
        public override void Interpret(Context context)
        {
            if (context.Formula.Contains("D"))
                context.TotalPoint += 20;
        }
    }
}
