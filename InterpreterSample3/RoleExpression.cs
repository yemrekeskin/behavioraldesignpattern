﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterpreterSample3
{
    // Expression
    abstract class RoleExpression
    {
        public abstract void Interpret(Context context);
    }
}
