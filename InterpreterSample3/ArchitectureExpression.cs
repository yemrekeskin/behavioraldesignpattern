﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterpreterSample3
{
    // TerminalExpression
    class ArchitectureExpression
        : RoleExpression
    {
        public override void Interpret(Context context)
        {
            if (context.Formula.Contains("A"))
            {
                context.TotalPoint += 5;
            }
        }
    }
}
