﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterpreterSample3
{
    // TerminalExpression
    class ConsultantExpression
        : RoleExpression
    {
        public override void Interpret(Context context)
        {
            if (context.Formula.Contains("C"))
                context.TotalPoint += 10;
        }
    }
}
