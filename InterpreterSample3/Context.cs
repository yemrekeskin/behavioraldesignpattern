﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterpreterSample3
{
    // Context class
    class Context
    {
        public string Formula { get; set; }
        public int TotalPoint { get; set; }
    }
}
