﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterpreterSample3
{
    // TerminalExpression
    class SeniorExpression
        : RoleExpression
    {
        public override void Interpret(Context context)
        {
            if (context.Formula.Contains("S"))
                context.TotalPoint += 15;
        }
    }
}
