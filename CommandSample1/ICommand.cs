﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommandSample1
{
    // The Command interface declares a method for executing a command.
    public interface ICommand
    {
        void Execute();
    }
}
