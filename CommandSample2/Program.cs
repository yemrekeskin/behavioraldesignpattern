﻿using System;
using System.Collections.Generic;

namespace CommandSample2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    public interface ICommand
    {
        void Execute();
    }

    //Command
    public abstract class CommandPerson
        : ICommand
    {
        private readonly IRepository _personRepository;

        public CommandPerson(IRepository repository)
        {
            this._personRepository = repository;
        }

        public abstract void Execute();
    }

    //Concrete Command
    public class CommandPersonCreate
        : CommandPerson
    {
        private readonly IRepository _personRepository;

        public CommandPersonCreate(IRepository repository)
            : base(repository)
        {
        }

        public override void Execute()
        {
            _personRepository.Create();
        }
    }

    public class CommandPersonDelete
        : CommandPerson
    {
        private readonly IRepository _personRepository;

        public CommandPersonDelete(IRepository repository)
            : base(repository)
        {
        }

        public override void Execute()
        {
            _personRepository.Delete();
        }
    }

    public class CommandPersonUpdate
        : CommandPerson
    {
        private readonly IRepository _personRepository;

        public CommandPersonUpdate(IRepository repository)
            : base(repository)
        {
        }

        public override void Execute()
        {
            _personRepository.Update();
        }
    }

    // Invorker
    public class CommandPersonInvorker
    {
        public List<CommandPerson> list { get; set; }

        public CommandPersonInvorker()
        {
            list = new List<CommandPerson>();
        }

        public void ExecuteAll()
        {
            if (list.Count == 0)
                return;

            foreach (CommandPerson item in list)
            {
                item.Execute();
            }
        }
    }

    //receiver
    public class PersonRepository
        : BaseRepository
    {
        public PersonRepository(BaseEntity p)
            : base(p)
        {
        }
    }

    public interface IRepository
    {
        bool Delete();

        bool Update();

        void Create();
    }

    public class BaseRepository
        : IRepository
    {
        private readonly BaseEntity entity;

        public BaseRepository(BaseEntity person)
        {
            this.entity = person;
        }

        public bool Delete()
        {
            Console.WriteLine("Deleted Person...");
            return true;
        }

        public bool Update()
        {
            Console.WriteLine("Updated Person...");
            return true;
        }

        public void Create()
        {
            Console.WriteLine("Created Person...");
        }
    }

    public abstract class BaseEntity
    {
        public int ID { get; set; }
    }

    public class Person
        : BaseEntity
    {
        public string Name { get; set; }
        public string Department { get; set; }
    }
}