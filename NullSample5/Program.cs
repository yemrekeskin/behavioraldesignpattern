﻿using System;
using System.Collections.Generic;

namespace NullSample5
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    public abstract class StaffBase
    {
        public abstract string IdentityNumber { get; set; }
        public abstract string FullName { get; set; }
        public abstract string Department { get; set; }

        public abstract int EfficiencyScore();

        public abstract Dictionary<int, string> TaskStatus();

        #region Null Implementation

        private static readonly NullStaff nullStaff = new NullStaff();

        public static NullStaff Null
        {
            get { return nullStaff; }
        }

        public class NullStaff
            : StaffBase
        {
            public override string IdentityNumber
            {
                get { return String.Empty; }
                set { }
            }

            public override string FullName
            {
                get { return String.Empty; }
                set { }
            }

            public override string Department
            {
                get { return String.Empty; }
                set { }
            }

            public override int EfficiencyScore()
            {
                // No operation or behaviour
                // But you can put operations depending on your case
                return 0;
            }

            public override Dictionary<int, string> TaskStatus()
            {
                // No operation or behaviour
                // But you can put operations depending on your case
                return null;
            }
        }

        #endregion Null Implementation
    }

    public class EngineerStaff
        : StaffBase
    {
        public override string IdentityNumber
        {
            get { return "G03" + System.Guid.NewGuid().ToString(); }
            set { }
        }

        public override string FullName
        {
            get { return "David Cameron"; }
            set { }
        }

        public override string Department
        {
            get { return "Enterprise Architect"; }
            set { }
        }

        public override int EfficiencyScore()
        {
            return 90;
        }

        public override Dictionary<int, string> TaskStatus()
        {
            return new Dictionary<int, string>();
        }
    }

    public class AnalystStaff
        : StaffBase
    {
        public override string IdentityNumber
        {
            get { return "G05" + System.Guid.NewGuid().ToString(); }
            set { }
        }

        public override string FullName
        {
            get { return "Imran Ozipek"; }
            set { }
        }

        public override string Department
        {
            get { return "Business Analyst"; }
            set { }
        }

        public override int EfficiencyScore()
        {
            return 80;
        }

        public override Dictionary<int, string> TaskStatus()
        {
            return new Dictionary<int, string>();
        }
    }

    // let'S use
    public class StaffRepository
    {
        public StaffBase Find(string identityNumber)
        {
            if (identityNumber.Contains("G05"))
                return new AnalystStaff();
            else if (identityNumber.Contains("G03"))
                return new EngineerStaff();

            return StaffBase.Null;
        }
    }
}