﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StateSample1
{
    // Concrete State tipi
    class WaitingState
        : VendingMachineState
    {
        public WaitingState()
        {
            Console.WriteLine("Waiting...");
        }
        public override void HandleState(VendingMachine context)
        {
            int totalProduct = context.ProductList.Sum<Product>(p => p.Count);

            Console.WriteLine("Makine bekleme konumunda. Şu anda {0} adet ürün var.", totalProduct.ToString());
            // Makine bekleme konumundayken  aslında bir State değişikliği söz konusu değil. Değişimi sağlayacak olan aslında istemcinin vereceği bir aksiyon. Context tipi üzerindeki RequestProduct metodunun çağırılması bu anlamda düşünülebilir.
        }
    }
}
