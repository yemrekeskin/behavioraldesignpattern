﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StateSample1
{
    // State tipi
    // abstract sınıf olabileceği gibi interface şeklinde de tasarlanabilir
    abstract class VendingMachineState
    {
        public abstract void HandleState(VendingMachine context);
    }
}
