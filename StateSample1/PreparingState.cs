﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StateSample1
{
    // Concrete State tipi
    class PreparingState : VendingMachineState
    {
        public PreparingState()
        {
            Console.WriteLine("Preparing...");
        }
        public override void HandleState(VendingMachine context)
        {
            Console.WriteLine("İstenilen ürün hazırlanıyor. Lütfen bekleyiniz");
            // Makienin durumu değiştiriliyor. Ürün hazırlanması bitmiş. Buna göre ürünü teslim etme durumuna geçiyor.
            context.State = new DeliveryState();
        }
    }
}
