﻿using System;

namespace StateSample1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // Context tipine ait nesne örneği oluşturulur
            VendingMachine machine = new VendingMachine();

            // İstemci bir ürün ister
            machine.RequestProduct("Çikolata K", 10);

            machine.RequestProduct("Bsissi", 12); // Bu ürün olmadığı için vermeyecektir. Herhangibir aksiyon alınmayacaktır.

            Console.ReadLine();
        }
    }
}
