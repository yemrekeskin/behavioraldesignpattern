﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StateSample1
{
    // Concrete State tipi
    class DeliveryState
          : VendingMachineState
    {
        public DeliveryState()
        {
            Console.WriteLine("Delivering...");
        }
        public override void HandleState(VendingMachine context)
        {
            Console.WriteLine("Ürün teslim ediliyor");
            // Makinin durumu değiştiriliyor. Ürün teslim edildikten sonra tekrar bekleme konumuna alınıyor.
            context.State = new WaitingState();
        }
    }
}
