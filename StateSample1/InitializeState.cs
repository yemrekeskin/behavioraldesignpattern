﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace StateSample1
{
    // Concrete State tipi
    // Otomat start düğmesine basılarak çalıştırıldığında öncelikli olarak bir ön hazırlık yapacaktır.
    class InitializeState
        : VendingMachineState
    {
        public InitializeState()
        {
            Console.WriteLine("Initialize...");
        }
        public override void HandleState(VendingMachine context)
        {
            Console.WriteLine("Ön hazırlıklar yapılıyor");
            Thread.Sleep(2000);
            // Makinenin durumu değiştiriliyor. Makine initialize edilmiş. Bekleme konumuna geçebilir.
            context.State = new WaitingState();
        }
    }
}
