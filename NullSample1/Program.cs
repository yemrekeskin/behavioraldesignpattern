﻿using System;

namespace NullSample1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    public class MobileRepository
    {
        public IMobile GetMobileByName(string mobileName)
        {
            IMobile mobile = NullMobile.Instance;
            switch (mobileName)
            {
                case "sony":
                    mobile = new SonyXperia();
                    break;

                case "apple":
                    mobile = new AppleIPhone();
                    break;

                case "samsung":
                    mobile = new SamsungGalaxy();
                    break;
            }
            return mobile;
        }
    }

    public interface IMobile
    {
        void TurnOn();
        void TurnOff();
    }

    public class SonyXperia : IMobile
    {
        public void TurnOff()
        {
            throw new NotImplementedException();
        }

        public void TurnOn()
        {
            throw new NotImplementedException();
        }
    }

    public class AppleIPhone : IMobile
    {
        public void TurnOff()
        {
            throw new NotImplementedException();
        }

        public void TurnOn()
        {
            throw new NotImplementedException();
        }
    }

    //mobile type implementing IMobile interface  
    public class SamsungGalaxy : IMobile
    {
        public void TurnOff()
        {
            Console.WriteLine("\nSamsung Galaxy Turned OFF!");
        }

        public void TurnOn()
        {
            Console.WriteLine("\nSamsung Galaxy Turned ON!");
        }
    }

    //our null object class implementing IMobile interface as a singleton  
    public class NullMobile : IMobile
    {
        private static NullMobile _instance;
        private NullMobile()
        { }

        public static NullMobile Instance
        {
            get
            {
                if (_instance == null)
                    return new NullMobile();
                return _instance;
            }
        }

        //do nothing methods  
        public void TurnOff()
        { }

        public void TurnOn()
        { }
    }
}
